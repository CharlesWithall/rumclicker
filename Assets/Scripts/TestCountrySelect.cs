﻿using UnityEngine;
using System.Collections;

public class TestCountrySelect : MonoBehaviour {

    public ECountry country;

    void OnMouseUp()
    {
        if (Globals.UnlockedCountries.Contains(country) && (!Globals.TutorialDisabled || Globals.TutorialDisable == ETutorialStage.MoveHome))
        {
            GameObject.Find("Flag").GetComponent<FlagScript>().currentHome = country;
        }
    }
}
