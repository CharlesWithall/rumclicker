﻿using UnityEngine;
using System.Collections;

public class TradeRouteMarker : MonoBehaviour {

    private Color startColor;
    public GameObject TradeRouteOptionScript;

	// Use this for initialization
	void Start ()
    {
        startColor = GetComponent<SpriteRenderer>().color;
    }
	
	// Update is called once per frame
	void Update () {
        
	    if (TradeRouteOptionScript.transform.parent.gameObject.activeSelf == true)
        {
            if (TradeRouteOptionScript.GetComponent<TradeRoute>().isPurchaseble)
            {
                Color32 Green = new Color32(0, 255, 0, 162);
                GetComponent<SpriteRenderer>().color = Green;
            }
            else
            {
                Color32 Red = new Color32(255, 0, 0, 162);
                GetComponent<SpriteRenderer>().color = Red;
            }
        }
        else if (TradeRouteOptionScript.GetComponent<TradeRoute>().tradeRoutesEstablished == 0)
        {
            Color tempColor = GetComponent<SpriteRenderer>().color;
            tempColor.a = 0;
            GetComponent<SpriteRenderer>().color = tempColor;
        }
        else
        {
            GetComponent<SpriteRenderer>().color = startColor;
        }
	}
}
