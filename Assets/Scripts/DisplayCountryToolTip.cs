﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Shows the tooltip when the player hovers over a country
/// </summary>
public class DisplayCountryToolTip : MonoBehaviour {

    private bool isUnlocked { get { return GetComponent<HighlightCountry>().isUnlocked; } }

    /// <summary>
    /// Executes on start of application
    /// </summary>
    void Start()
    {
        transform.Find("ToolTip").gameObject.GetComponent<Text>().text = gameObject.name;
    }

    /// <summary>
    /// Executes when the mouse enters the target's space
    /// </summary>
    void OnMouseEnter()
    {
        if (isUnlocked && (!Globals.TutorialDisabled || Globals.TutorialDisable == ETutorialStage.MoveHome))
        {
            StartCoroutine("WaitForShowTooltip");
        }
    }

    /// <summary>
    /// Executes when the mouse leaves the target's space
    /// </summary>
    void OnMouseExit()
    {
        if (isUnlocked && (!Globals.TutorialDisabled || Globals.TutorialDisable == ETutorialStage.MoveHome))
        {
            StopCoroutine("WaitForShowTooltip");
            transform.Find("ToolTip").gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Waits for a bit before showing the tooltip
    /// </summary>
    /// <returns>Wait for a number of seconds then set tooltip text to active</returns>
    IEnumerator WaitForShowTooltip()
    {
        yield return new WaitForSeconds(0.7f);
        transform.Find("ToolTip").gameObject.SetActive(true);
    }
}
