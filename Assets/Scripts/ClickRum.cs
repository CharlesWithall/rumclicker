﻿using UnityEngine;

/// <summary>
/// Handles the player's interraction with the rum bottles
/// </summary>
public class ClickRum : MonoBehaviour {

    public ERumType RumType;
    public static bool halfSpeed;
    public static bool doubleSpeed;
    private ETutorialStage tutorialStage { get { return RumType == ERumType.LightRum ? ETutorialStage.BuyLightRum : ETutorialStage.BuyDarkRum; } }
    private int multiplier { get { return doubleSpeed ? 2 : 1; } }
    private bool isAudioActive;
    private float audioActivationCountdown = 0;

    /// <summary>
    /// The action to perform when the mouse enters the object's space
    /// </summary>
    void OnMouseEnter()
    {
        if (!isAudioActive && (!Globals.TutorialDisabled || Globals.TutorialDisable == tutorialStage))
        {
            isAudioActive = true;
            GetComponent<AudioSource>().Play();
        }
    }

    /// <summary>
    /// Called every frame
    /// </summary>
    void Update()
    {
        if (isAudioActive)
        {
            if (audioActivationCountdown >= 2.5)
            {
                isAudioActive = false;
            }

            audioActivationCountdown += Time.deltaTime;
        }
        else
        {
            audioActivationCountdown = 0;
        }

        // SHOW TUTORIAL - BUY TRADE ROUTE
        if (!TutorialSlave.GetTutorialStageShown(ETutorialStage.BuyTradeRoute) && Globals.LightRumQuantity >= 15)
        {
            GameObject.Find("TutorialUI").GetComponent<TutorialController>().ShowTutorial(ETutorialStage.BuyTradeRoute);
        }

        // SHOW TUTORIAL - Buy Upgrade
        if (!TutorialSlave.GetTutorialStageShown(ETutorialStage.BuyUpgrade) && TutorialSlave.GetTutorialStageShown(ETutorialStage.MoveHome) && Globals.LightRumQuantity >= 100)
        {
            GameObject.Find("TutorialUI").GetComponent<TutorialController>().ShowTutorial(ETutorialStage.BuyUpgrade);
        }

        // SHOW TUTORIAL - ChangeWeather
        if (!TutorialSlave.GetTutorialStageShown(ETutorialStage.ChangeWeather) && Globals.LightRumQuantity >= 500)
        {
            GameObject.Find("TutorialUI").GetComponent<TutorialController>().ShowTutorial(ETutorialStage.ChangeWeather);
        }
    }

    /// <summary>
    /// The action to perform when the mouse button is released
    /// </summary>
    void OnMouseUp()
    {
        if (!Globals.TutorialDisabled || Globals.TutorialDisable == tutorialStage)
        {
            for (int i = 0; i < multiplier; i++)
            {
                switch (RumType)
                {
                    case ERumType.DarkRum:
                        Globals.DarkRumQuantity += Globals.HullUpgradeMultiplier;
                        break;
                    case ERumType.LightRum:
                        Globals.LightRumQuantity += Globals.HullUpgradeMultiplier;
                        break;
                }
            }
        }
    }

    /// <summary>
    /// The action to perform when the mouse button is depressed
    /// </summary>
    void OnMouseDown()
    {
        if (!Globals.TutorialDisabled || Globals.TutorialDisable == tutorialStage)
        {
            GameObject.Find("Click").GetComponent<AudioSource>().Play();

            if (!halfSpeed)
            {
                for (int i = 0; i < multiplier; i++)
                {
                    switch (RumType)
                    {
                        case ERumType.DarkRum:
                            Globals.DarkRumQuantity += Globals.HullUpgradeMultiplier;
                            break;
                        case ERumType.LightRum:
                            Globals.LightRumQuantity += Globals.HullUpgradeMultiplier;
                            break;
                    }
                }
            }
        }

        // HIDE TUTORIAL - BUY LIGHT RUM
        if (Globals.TutorialDisable == ETutorialStage.BuyLightRum)
        {
            GameObject.Find("TutorialUI").GetComponent<TutorialController>().HideTutorial(ETutorialStage.BuyLightRum);
        }

        // HIDE TUTORIAL - BUY DARK RUM
        if (Globals.TutorialDisable == ETutorialStage.BuyDarkRum)
        {
            GameObject.Find("TutorialUI").GetComponent<TutorialController>().HideTutorial(ETutorialStage.BuyDarkRum);
        }
    }
}
