﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles the building upgrades
/// </summary>
public class BuildingUpgrade : MonoBehaviour
{
    uint currentIndex { get { return Globals.CountryDevelopment[GameObject.Find("Flag").GetComponent<FlagScript>().currentHome]; } }
    long[] lightPrices;
    long[] darkPrices;
    string[] buildingNames;
    public Sprite[] buildingIcons;

    private bool isPurchaseble { get { return (hasLastUpgradeBeenPurchased ? false : Globals.LightRumQuantity >= BuildCostSlave.GetCost(GameObject.Find("Flag").GetComponent<FlagScript>().currentHome, currentIndex)); } }
    private bool hasLastUpgradeBeenPurchased = false;

    public GameObject thisButton;
    public GameObject BuildingIcon;

    /// <summary>
    /// Executed on start of application
    /// </summary>
    void Start()
    {
        lightPrices = new long[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        darkPrices = new long[10] { 100, 500, 10000, 100000, 10000000, 100000000, 1000000000, 10000000000, 10000000000000, 100000000000000 };
        buildingNames = new string[7] { "Build Tavern", "Build Distillery", "Build Harbour", "Build Plantation", "Build Dockyard", "Build Embassy", "Fully Upgraded" };
    }

    /// <summary>
    /// Purchases next building in the updgrade queue
    /// </summary>
    public void UpgradeBuilding()
    {
        if (isPurchaseble && !hasLastUpgradeBeenPurchased )
        {
            Globals.TradeUpgradePercentage *= 2;

            Globals.LightRumQuantity -= BuildCostSlave.GetCost(GameObject.Find("Flag").GetComponent<FlagScript>().currentHome, currentIndex);
           
            Globals.CountryDevelopment[GameObject.Find("Flag").GetComponent<FlagScript>().currentHome]++;

            if (currentIndex == buildingNames.Length - 1)
            {
                hasLastUpgradeBeenPurchased = true;
            }
            GetComponent<AudioSource>().Play();
        }       
    }

    /// <summary>
    /// Changes the image on the building upgrade panel
    /// </summary>
    void ChangeIcon()
    {
        if (currentIndex == buildingIcons.Length)
        {
            BuildingIcon.SetActive(false);
        }
        else
        {
            BuildingIcon.GetComponent<SpriteRenderer>().sprite = buildingIcons[currentIndex];
        }
    }

    /// <summary>
    /// Called every frame
    /// </summary>
    void Update()
    {
        lightPrices = new long[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        darkPrices = new long[10] { 100, 500, 10000, 100000, 10000000, 100000000, 1000000000, 10000000000, 10000000000000, 100000000000000 };
        buildingNames = new string[7] { "Build Tavern", "Build Distillery", "Build Harbour", "Build Plantation", "Build Dockyard", "Build Embassy", "Fully Upgraded" };

        transform.parent.Find("Text").gameObject.GetComponent<Text>().text = buildingNames[currentIndex];
        ChangeIcon();

        if (hasLastUpgradeBeenPurchased)
        {
            thisButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            if (isPurchaseble)
            {
                thisButton.GetComponent<Button>().interactable = true;
            }
            else
            {
                thisButton.GetComponent<Button>().interactable = false;
            }
        }
    }
}
