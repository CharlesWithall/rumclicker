﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FlagScript : MonoBehaviour {

    public ECountry currentHome;
    private ECountry previousHome;
    public List<Sprite> flagCollection;

	/// <summary>
    /// Executes on startup of application
    /// </summary>
	void Start () {
        currentHome = ECountry.Great_Britain;
        previousHome = ECountry.None;
        flagCollection = flagCollection.OrderBy(sprite => sprite.name).ToList();
	}
	
	/// <summary>
    /// Executes once per frame
    /// </summary>
	void Update () {
        UpdateImage();
	}

    /// <summary>
    /// Plays the flag sound effect
    /// </summary>
    void PlaySoundEffect()
    {
        if (previousHome != ECountry.None)
        {
            GetComponent<AudioSource>().Play();
        }
    }

    /// <summary>
    /// Changes the image to the newly selected country
    /// </summary>
    void UpdateImage()
    {
        if (previousHome != currentHome)
        {
            PlaySoundEffect();

            switch (currentHome)
            {
                case ECountry.Antigua:
                    GetComponent<SpriteRenderer>().sprite = flagCollection[0];
                    break;
                case ECountry.Bahamas:
                    GetComponent<SpriteRenderer>().sprite = flagCollection[1];
                    break;
                case ECountry.Cuba:
                    GetComponent<SpriteRenderer>().sprite = flagCollection[2];
                    break;
                case ECountry.Great_Britain:
                    GetComponent<SpriteRenderer>().sprite = flagCollection[3];
                    break;
                case ECountry.Hispaniola:
                    GetComponent<SpriteRenderer>().sprite = flagCollection[4];
                    break;
                case ECountry.Jamaica:
                    GetComponent<SpriteRenderer>().sprite = flagCollection[5];
                    break;
                case ECountry.Mexico:
                    GetComponent<SpriteRenderer>().sprite = flagCollection[6];
                    break;
                case ECountry.Nicaragua:
                    GetComponent<SpriteRenderer>().sprite = flagCollection[7];
                    break;
                case ECountry.Puerto_Rico:
                    GetComponent<SpriteRenderer>().sprite = flagCollection[8];
                    break;
                case ECountry.Trinidad:
                    GetComponent<SpriteRenderer>().sprite = flagCollection[9];
                    break;
                case ECountry.United_States:
                    GetComponent<SpriteRenderer>().sprite = flagCollection[10];
                    break;
                case ECountry.Venezuela:
                    GetComponent<SpriteRenderer>().sprite = flagCollection[11];
                    break;
                default:
                    throw new System.Exception("The flag is trying to change to a country that doesn't exist: " + currentHome.ToString());
            }

            previousHome = currentHome;
        }
    }
}
