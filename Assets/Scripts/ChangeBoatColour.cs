﻿using UnityEngine;
using System;


/// <summary>
/// Changes the colour of the boat and what rum is produces
/// </summary>
public class ChangeBoatColour : MonoBehaviour {

    public GameObject PartnerBoat;

    /// <summary>
    /// The action to perform when the mouse button is depressed
    /// </summary>
    void OnMouseDown()
    {
        if (!Globals.TutorialDisabled && TutorialSlave.GetTutorialStageShown(ETutorialStage.BuyDarkRum))
        {
            GameObject.Find("Click").GetComponent<AudioSource>().Play();
        }
    }

    /// <summary>
    /// The action to perform when the mouse button is released
    /// </summary>
    void OnMouseUp()
    {
        if (!Globals.TutorialDisabled && TutorialSlave.GetTutorialStageShown(ETutorialStage.BuyDarkRum))
        {
            ChangeColour();
        }
    }

    /// <summary>
    /// Change the colour of the boat
    /// </summary>
    public void ChangeColour()
    {
        GetComponent<FollowRoute>().isDarkRum = !GetComponent<FollowRoute>().isDarkRum;

        int currentAnimIndex = Array.IndexOf(GameObject.Find("AnimationSlave").GetComponent<AnimationSlave>().AnimationPool, GetComponent<Animator>().runtimeAnimatorController);
        int indexAddition = 5;

        if (currentAnimIndex >= 5)
        {
            indexAddition = -indexAddition;
        }
        GetComponent<Animator>().runtimeAnimatorController = GameObject.Find("AnimationSlave").GetComponent<AnimationSlave>().AnimationPool[currentAnimIndex + indexAddition];

        if (GetComponent<FollowRoute>().isDarkRum)
        {
            if (PartnerBoat.GetComponent<SpriteRenderer>().enabled == false || PartnerBoat.GetComponent<FollowRoute>().isDarkRum)
            {
                GameObject.Find("TradeRouteHolder").GetComponent<TradeRouteHandler>().ChangeCoroutineVariables(GetComponent<FollowRoute>().routeName, changeToDark: true);
            }
            else
            {
                GameObject.Find("TradeRouteHolder").GetComponent<TradeRouteHandler>().ChangeCoroutineVariables(GetComponent<FollowRoute>().routeName, changeToHalf: true);
            }
        }
        else
        {
            if (PartnerBoat.GetComponent<SpriteRenderer>().enabled == false || !PartnerBoat.GetComponent<FollowRoute>().isDarkRum)
            {
                GameObject.Find("TradeRouteHolder").GetComponent<TradeRouteHandler>().ChangeCoroutineVariables(GetComponent<FollowRoute>().routeName, changeToLight: true);
            }
            else
            {
                GameObject.Find("TradeRouteHolder").GetComponent<TradeRouteHandler>().ChangeCoroutineVariables(GetComponent<FollowRoute>().routeName, changeToHalf: true);
            }
        }
    }
}
