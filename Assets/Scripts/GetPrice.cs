﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Gets the price of the rum as an appropriate string
/// </summary>
public class GetPrice : MonoBehaviour {

    public ERumType rumType;

    /// <summary>
    /// Changes the text for the price of the rum to an aesthetically appropriate string
    /// </summary>
    void Update()
    {
        switch (rumType)
        {
            case ERumType.LightRum:
                GetComponent<Text>().text = Globals.FormatLargeNumber(GetComponentInParent<TradeRoute>().lightPrice);
                break;
            case ERumType.DarkRum:
                GetComponent<Text>().text = Globals.FormatLargeNumber(GetComponentInParent<TradeRoute>().darkPrice);
                break;
        }
    }
}
