﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class TutorialController : MonoBehaviour {

    public bool ShowUI;
    public GameObject Mask;
    public GameObject Highlight;
    public bool isReadyToClose = true;
    private bool GetBigger;
    private bool GetSmaller;
    private float speedOfIncrease = 300;
    private bool tutorialOneShown = false;
    public AudioClip[] SoundEffects;
    public GameObject[] ObjectsToReveal;

    void Start()
    {
        transform.localScale = new Vector3(0.5f, 0.5f, 1);
        ShowUI = false;
        
        // SHOW TUTORIAL - BUY LIGHT RUM
        ShowTutorial(ETutorialStage.BuyLightRum);
    }

	// Update is called once per frame
	void Update () {

	    if (transform.localScale.x <= 0.5 && isReadyToClose)
        {
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponentInChildren<Canvas>().enabled = false;
            GetSmaller = false;
            isReadyToClose = false;
            GetComponent<DampedHarmonicOscillation>().enabled = true;
            GetComponent<DampedHarmonicOscillation>().lengthOfProcess = 0.1f;
            GetComponent<DampedHarmonicOscillation>().isGettingBigger = false;
        }

        if (ShowUI && GetBigger == false && GetComponent<DampedHarmonicOscillation>().isGettingBigger == false)
        {
            GetComponent<SpriteRenderer>().enabled = true;
            GetComponentInChildren<Canvas>().enabled = true;
            GetBigger = true;
            //StartCoroutine(DisableMenuClose(4));
            isReadyToClose = true;
        }

        if (GetBigger)
        {
            float x = transform.localScale.x;
            x += Time.deltaTime * speedOfIncrease;
            transform.localScale = new Vector3(x, x, 1);
        }

        if (transform.localScale.x >= 80 && GetBigger)
        {
            GetBigger = false;
            GetComponent<DampedHarmonicOscillation>().lengthOfProcess = 4;
            GetComponent<DampedHarmonicOscillation>().isGettingBigger = true;
        }

        if (!ShowUI && isReadyToClose)
        {
            GetSmaller = true;
        }

        if (GetSmaller)
        {
            float x = transform.localScale.x;
            x -= Time.deltaTime * speedOfIncrease;
            transform.localScale = new Vector3(x, x, 1);
        }
	}

    IEnumerator DisableMenuClose(float duration)
    {
        yield return new WaitForSeconds(duration);
        isReadyToClose = true;
    }

    public void ShowTutorial(ETutorialStage tutorialStage)
    {
        if (!ShowUI)
        {
            // ENABLE WEATHER
            if(tutorialStage == ETutorialStage.ChangeWeather)
            {
                GameObject.Find("WeatherIcon").GetComponent<WeatherSwitcher>().ChangeWeather();
            }

            EnableAllRenderers(tutorialStage);

            GetComponent<AudioSource>().clip = SoundEffects[0];
            GetComponent<AudioSource>().Play();
            Mask.SetActive(true);
            Highlight.transform.localPosition = TutorialSlave.GetTutorialPosition(tutorialStage);
            Highlight.transform.localScale = TutorialSlave.GetTutorialSize(tutorialStage);
            GetComponentInChildren<Text>().text = TutorialSlave.GetTutorialText(tutorialStage);

            foreach (var s in TutorialSlave.GetTutorialGameObjects(tutorialStage))
            {
                if (s != null)
                {
                    if (GameObject.Find(s).GetComponent<SpriteRenderer>() != null)
                    {
                        GameObject.Find(s).GetComponent<SpriteRenderer>().sortingOrder += 50;
                    }
                    else if (GameObject.Find(s).GetComponent<Canvas>() != null)
                    {
                        GameObject.Find(s).GetComponent<Canvas>().sortingOrder += 50;
                    }

                    if (GameObject.Find(s).GetComponentInChildren<SpriteRenderer>() != null && tutorialStage == ETutorialStage.BuyUpgrade)
                    {
                        GameObject.Find(s).GetComponentInChildren<SpriteRenderer>().sortingOrder += 50;
                    }    
                }
            }

            ShowUI = true;
            Highlight.SetActive(true);
            Globals.TutorialDisable = tutorialStage;
        }
    }

    public void HideTutorial(ETutorialStage tutorialStage)
    {
        if (ShowUI && isReadyToClose)
        {
            GetComponent<AudioSource>().clip = SoundEffects[1];
            GetComponent<AudioSource>().Play();
            foreach (var s in TutorialSlave.GetTutorialGameObjects(tutorialStage))
            {
                if (s != null)
                {
                    if (GameObject.Find(s).GetComponent<SpriteRenderer>() != null)
                    {
                        GameObject.Find(s).GetComponent<SpriteRenderer>().sortingOrder -= 50;
                    }
                    else if (GameObject.Find(s).GetComponent<Canvas>() != null)
                    {
                        GameObject.Find(s).GetComponent<Canvas>().sortingOrder -= 50;
                    }

                    if (GameObject.Find(s).GetComponentInChildren<SpriteRenderer>() != null && tutorialStage == ETutorialStage.BuyUpgrade)
                    {
                        GameObject.Find(s).GetComponentInChildren<SpriteRenderer>().sortingOrder -= 50;
                    }
                }
            }
            Mask.SetActive(false);
            Highlight.SetActive(false);
            ShowUI = false;
            TutorialSlave.SetTutorialStageShown(tutorialStage);
            Globals.TutorialDisable = ETutorialStage.NONE;
            GetComponent<DampedHarmonicOscillation>().enabled = false;
        }
    }

    private void EnableAllRenderers(ETutorialStage tutorialStage)
    {
        foreach (var index in ObjectRevealKey(tutorialStage))
        {
            if (tutorialStage == ETutorialStage.BuyTradeRoute || tutorialStage == ETutorialStage.BuyUpgrade)
            {
                ObjectsToReveal[index].SetActive(true);
            }
            else
            {
                if (ObjectsToReveal[index].GetComponent<Renderer>() != null)
                {
                    ObjectsToReveal[index].GetComponent<Renderer>().enabled = true;
                }

                foreach (var renderer in ObjectsToReveal[index].GetComponentsInChildren<Renderer>())
                {
                    renderer.enabled = true;
                }
            }

            if (tutorialStage == ETutorialStage.BuyDarkRum)
            {
                if (ObjectsToReveal[index].GetComponentInChildren<Text>() != null)
                {
                    ObjectsToReveal[index].GetComponentInChildren<Text>().enabled = true;
                }
            }
        }
    }

    private List<int> ObjectRevealKey(ETutorialStage tutorialStage)
    {
        List<int> indices = new List<int>();
        switch (tutorialStage)
        {
            case ETutorialStage.BuyDarkRum:
                indices.Add(0);
                indices.Add(1);
                break;
            case ETutorialStage.BuyTradeRoute:
                indices.Add(2);
                break;
            case ETutorialStage.BuyUpgrade:
                indices.Add(3);
                break;
            case ETutorialStage.ChangeWeather:
                indices.Add(4);
                break;
        }

        return indices;
    }
}
