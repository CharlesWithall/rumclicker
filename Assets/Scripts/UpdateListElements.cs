﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UpdateListElements : MonoBehaviour {

    public GameObject flag;
    private ECountry previousHome;

    void Start()
    {
        previousHome = ECountry.None;
    }

	// Update is called once per frame
	void Update () {
        if (flag.GetComponent<FlagScript>().currentHome != previousHome)
        {
            foreach (Transform child in transform)
            {
                if (child.GetComponentInChildren<TradeRoute>().country_A == flag.GetComponent<FlagScript>().currentHome)                    
                {
                    child.gameObject.SetActive(true);
                    child.GetComponentInChildren<Text>().text = string.Format("{0} - {1}", NormaliseString(child.GetComponentInChildren<TradeRoute>().country_A), NormaliseString(child.GetComponentInChildren<TradeRoute>().country_B));
                }
                else if (child.GetComponentInChildren<TradeRoute>().country_B == flag.GetComponent<FlagScript>().currentHome)
                {
                    child.gameObject.SetActive(true);
                    child.GetComponentInChildren<Text>().text = string.Format("{0} - {1}", NormaliseString(child.GetComponentInChildren<TradeRoute>().country_B), NormaliseString(child.GetComponentInChildren<TradeRoute>().country_A));
                }
                else
                {
                    child.gameObject.SetActive(false);
                }
            }

            previousHome = flag.GetComponent<FlagScript>().currentHome;
        }
	}

    string NormaliseString(ECountry country)
    {
        string str = country.ToString();
        str = str.Replace('_', ' ');
        return str;
    }
}
