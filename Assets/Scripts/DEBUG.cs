﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class DEBUG : MonoBehaviour {

    bool isEnabled = false;

    /// <summary>
    /// Q = + 100 of both types of rum
    /// W = + 1,000 of both types of rum
    /// E = + 10,000 of both types of rum
    /// R = + 100,000 of both types of rum
    /// T = + 1 million of both types of rum
    /// Y = + 1 trillion of both types of rum
    /// U = Unlock all countries
    /// I = Adds 10 coroutines for every trade route
    /// A = Start a Trade Route giving 1 rum per second
    /// S = Start a Trade Route giving a million rum per second
    /// D = Change Weather at random
    /// F = Change Flag at random
    /// G = Show Tutorial Thing
    /// H = Hide Tutorial Thing
    /// </summary>
    void Update()
    {
        if (isEnabled)
        {
            if (Input.GetKeyUp(KeyCode.Q))
            {
                Globals.LightRumQuantity += 100;
                Globals.DarkRumQuantity += 100;
            }
            if (Input.GetKeyUp(KeyCode.W))
            {
                Globals.LightRumQuantity += 1000;
                Globals.DarkRumQuantity += 1000;
            }
            if (Input.GetKeyUp(KeyCode.E))
            {
                Globals.LightRumQuantity += 10000;
                Globals.DarkRumQuantity += 10000;
            }
            if (Input.GetKeyUp(KeyCode.R))
            {
                Globals.LightRumQuantity += 100000;
                Globals.DarkRumQuantity += 100000;
            }
            if (Input.GetKeyUp(KeyCode.T))
            {
                Globals.LightRumQuantity += 1000000;
                Globals.DarkRumQuantity += 1000000;
            }
            if (Input.GetKeyUp(KeyCode.Y))
            {
                Globals.LightRumQuantity += 1000000000000;
                Globals.DarkRumQuantity += 1000000000000;
            }
            if (Input.GetKeyUp(KeyCode.U))
            {
                foreach (ECountry country in Enum.GetValues(typeof(ECountry)))
                {
                    if (!Globals.UnlockedCountries.Contains(country))
                    {
                        Globals.UnlockedCountries.Add(country);
                    }
                }
            }
            if (Input.GetKeyUp(KeyCode.I))
            {
                foreach (Transform child in GameObject.Find("RouteGrid").gameObject.transform)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        child.GetComponentInChildren<TradeRoute>().BuyTradeRoute();
                    }
                }
            }
            if (Input.GetKeyUp(KeyCode.A))
            {
                GameObject.Find("TradeRouteHolder").GetComponent<TradeRouteHandler>().StartTradeRouteCoroutine(ERouteName.AntUK, 1, false);
            }
            if (Input.GetKeyUp(KeyCode.S))
            {
                GameObject.Find("TradeRouteHolder").GetComponent<TradeRouteHandler>().StartTradeRouteCoroutine(ERouteName.MexNic, 1000000, false);
            }
            if (Input.GetKeyUp(KeyCode.D))
            {
                CallPrivateMethod(GameObject.Find("WeatherIcon").GetComponent<WeatherSwitcher>(), "ChangeWeather");
            }
            if (Input.GetKeyUp(KeyCode.F))
            {
                System.Random rand = new System.Random();
                int index = rand.Next(12);
                GameObject.Find("Flag").GetComponent<FlagScript>().currentHome = (ECountry)index;
            }
        }
    }

    private void CallPrivateMethod(object o, string methodName, params object[] args)
    {
        var mi = o.GetType().GetMethod(methodName, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
        if (mi != null)
        {
            mi.Invoke(o, args);
        }
    }

    int i = 0;
}
