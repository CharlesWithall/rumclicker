﻿/// <summary>
/// Weather conditions
/// </summary>
public enum EWeather {

	Rainy,
    Sunny,
    Windy,
}
