﻿/// <summary>
/// Combinations of countries' three letter abbreviations
/// </summary>
public enum ERouteName {

	BahCub,
    PueTri,
    NicVen,
    MexNic,
    JamVen,
    AntUK,
    MexUSA,
    CubHis,
    HisJam,
    HisVen,
    JamNic,
    JamMex,
    CubUSA,
    HisPue,
    TriVen,
    AntPue,
    CubMex,
    BahPue,
    BahUSA,
    AntTri,
}
