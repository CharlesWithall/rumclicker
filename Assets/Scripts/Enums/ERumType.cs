﻿/// <summary>
/// Different types of rum
/// </summary>
public enum ERumType
{
    LightRum,
    DarkRum
}

