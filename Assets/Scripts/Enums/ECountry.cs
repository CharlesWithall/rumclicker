﻿/// <summary>
/// All Countries in the game and 'NONE' to indicate no country selected
/// </summary>
public enum ECountry
{   
    Antigua,
    Bahamas,
    Cuba,
    Great_Britain,
    Hispaniola,
    Jamaica,
    Mexico,
    Nicaragua,
    Puerto_Rico,
    Trinidad,
    United_States,
    Venezuela,
    None
}

