﻿/// <summary>
/// Steps in the tutorial flow
/// </summary>
public enum ETutorialStage {

	BuyLightRum,
    BuyTradeRoute,
    MoveHome,
    ChangeWeather,
    BuyDarkRum,
    BuyUpgrade,
    NONE
}
