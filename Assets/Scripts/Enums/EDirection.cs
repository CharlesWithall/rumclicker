﻿/// <summary>
/// Eight directions of the compass
/// </summary>
public enum EDirection
{
    North,
    NorthEast,
    East,
    SouthEast,
    South,
    SouthWest,
    West,
    NorthWest
}
