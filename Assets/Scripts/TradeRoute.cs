﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Linq;

public class TradeRoute : MonoBehaviour {

    double baseRumPerSecond = 0;
    double timeValue { get { return Globals.WeatherTradeRouteModifier; } } // How often in seconds rum gets traded
    bool boat0IsDark { get { return boats[0].GetComponent<FollowRoute>().isDarkRum; } }
    bool boat1IsDark { get { return boats[1].activeSelf ? boats[1].GetComponent<FollowRoute>().isDarkRum : boat0IsDark; } }
    float ticker = 0;
    uint buildingMultiplierIndex { get { return Globals.CountryDevelopment[country_A] + Globals.CountryDevelopment[country_B]; } }

    public GameObject thisButton;
    public ECountry country_A;
    public ECountry country_B;
    public ERouteName routeName;
    public uint tradeRoutesEstablished;
    public double lightPrice;
    public double darkPrice;
    private const double priceModifier = 1.15;
    public uint RumPerSecond;
    public GameObject[] boats;
    private bool tutorialTwoShown = false;
    private bool tutorialThreeShown = false;

    public bool isPurchaseble { get { return (Globals.LightRumQuantity >= lightPrice && Globals.DarkRumQuantity >= darkPrice); } }

    void Start()
    {
        //lightPrice = Dictionaries.LightPrices.Single(item => item.Key == routeName).Value;
        //darkPrice = Dictionaries.DarkPrices.Single(item => item.Key == routeName).Value;
    }

    public void BuyTradeRoute()
    {
        // HIDE TUTORIAL - BUY TRADE ROUTE
        if (Globals.TutorialDisable == ETutorialStage.BuyTradeRoute)
        {
            GameObject.Find("TutorialUI").GetComponent<TutorialController>().HideTutorial(ETutorialStage.BuyTradeRoute);
        }

        if (!Globals.TutorialDisabled || Globals.TutorialDisable == ETutorialStage.BuyTradeRoute)
        {
            tradeRoutesEstablished++;

            baseRumPerSecond += RumPerSecond;

            Globals.LightRumQuantity -= lightPrice;
            Globals.DarkRumQuantity -= darkPrice;
            lightPrice = (uint)Math.Round(lightPrice * priceModifier);
            darkPrice = (uint)Math.Round(darkPrice * priceModifier);

            UnlockCountry();
            RevealNewBoat();
        }

        // SHOW TUTORIAL - Move Home
        if (!TutorialSlave.GetTutorialStageShown(ETutorialStage.MoveHome) && tradeRoutesEstablished >= 5)
        {
            GameObject.Find("TutorialUI").GetComponent<TutorialController>().ShowTutorial(ETutorialStage.MoveHome);
        }
    }

    private void UnlockCountry()
    {
        if (!Globals.UnlockedCountries.Contains(country_A))
        {
            if (country_A != ECountry.Antigua || tradeRoutesEstablished >= 5)
            {
                Globals.UnlockedCountries.Add(country_A);
            }
        }
        if (!Globals.UnlockedCountries.Contains(country_B))
        {
            if (country_B != ECountry.Antigua || tradeRoutesEstablished >= 5)
            {
                Globals.UnlockedCountries.Add(country_B);
            }
        }
    }

    private void RevealNewBoat()
    {
        if (tradeRoutesEstablished == 1)
        {
            foreach(var boat in boats)
            {
                boat.SetActive(true);
                boat.GetComponent<SpriteRenderer>().enabled = false;
            }

            boats[0].GetComponent<SpriteRenderer>().enabled = true;
        }
        else if (tradeRoutesEstablished == 10)
        {
            boats[1].GetComponent<SpriteRenderer>().enabled = true;
            if (boats[1].GetComponent<ChangeBoatColour>().PartnerBoat.GetComponent<FollowRoute>().isDarkRum)
            {
                boats[1].GetComponent<ChangeBoatColour>().ChangeColour();
            }
        }
    }

    private void SetTradeRouteCountText()
    {
        transform.parent.Find("Quantity").gameObject.GetComponent<Text>().text = tradeRoutesEstablished.ToString();
    }

    void Update()
    {
        if (isPurchaseble)
        {
            thisButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            thisButton.GetComponent<Button>().interactable = false;
        }

        SetTradeRouteCountText();

        // Actually running the trade route
        if (ticker >= timeValue)
        {
            EstablishTradeRoute();
            ticker = 0;
        }
        else
        {
            ticker += Time.deltaTime;
        }
    }

    void EstablishTradeRoute()
    {
        double actualRumPerSecond = (baseRumPerSecond + (baseRumPerSecond * (Globals.TradeUpgradePercentage / 100))) * Math.Pow(2, buildingMultiplierIndex);

        if (boat0IsDark && boat1IsDark)
        {
            Globals.DarkRumQuantity += actualRumPerSecond;
        }
        else if (!boat0IsDark && !boat1IsDark)
        {
            Globals.LightRumQuantity += actualRumPerSecond;
        }
        else
        {
            Globals.LightRumQuantity += actualRumPerSecond / 2;
            Globals.DarkRumQuantity += actualRumPerSecond / 2;
        }
    }

   
}
