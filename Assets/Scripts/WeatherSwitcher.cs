﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class WeatherSwitcher : MonoBehaviour {

    public Sprite[] weatherIcons;
    public AudioClip[] SoundEffects;
    Sprite currentWeather;
    public EWeather enumWeather;
    int currentMinute;
    public float BoatSpeed;
    private bool isAnimating;
    private bool weatherHasSwitched;
    private float animatedSpeed = 150;
    private string toolTipText;

    void Start()
    {
        weatherHasSwitched = false;
        SetStartingWeather();
        isAnimating = false;
        currentMinute = DateTime.UtcNow.Minute;
        GetComponent<SpriteRenderer>().sprite = currentWeather;
        transform.Find("ToolTip").gameObject.GetComponent<Text>().text = GetToolTipText();
    }

	// Update is called once per frame
	void Update () {
        if (DateTime.UtcNow.Minute != currentMinute && TutorialSlave.GetTutorialStageShown(ETutorialStage.ChangeWeather) && !Globals.TutorialDisabled) // && DateTime.UtcNow.Minute % 5 == 0
        {
            ChangeWeather();
            PlaySoundEffect();
            isAnimating = true;
        }
        currentMinute = DateTime.UtcNow.Minute;

        if (isAnimating)
        {
            transform.Rotate(Vector3.up * Time.deltaTime * animatedSpeed);
            if (transform.localEulerAngles.y >= 90 && !weatherHasSwitched)
            {
                GetComponent<SpriteRenderer>().sprite = currentWeather;
                transform.localEulerAngles = new Vector3(0, 270, 0);
                weatherHasSwitched = true;
                transform.Find("ToolTip").gameObject.GetComponent<Text>().text = GetToolTipText();
            }

            if (transform.localEulerAngles.y <= 180 && weatherHasSwitched)
            {
                transform.localEulerAngles = new Vector3(0, 0, 0);
                isAnimating = false;
                weatherHasSwitched = false;
            }
        }     
	}

    void SetStartingWeather()
    {
        currentWeather = weatherIcons[1];
        BoatSpeed = 30f;
        Globals.WeatherTradeRouteModifier = 1;
        ClickRum.doubleSpeed = false;
        ClickRum.halfSpeed = true;
        enumWeather = EWeather.Sunny;
    }

    public void ChangeWeather()
    {
        System.Random rng = new System.Random();
        int index = rng.Next(weatherIcons.Length);
        currentWeather = weatherIcons[index];

        switch(currentWeather.name)
        {
            case "weather_rain":
                BoatSpeed = 20f;
                Globals.WeatherTradeRouteModifier = 1.15f;
                ClickRum.doubleSpeed = true;
                ClickRum.halfSpeed = false;
                enumWeather = EWeather.Rainy;
                break;
            case "weather_sun":
                BoatSpeed = 30f;
                Globals.WeatherTradeRouteModifier = 1;
                ClickRum.doubleSpeed = false;
                ClickRum.halfSpeed = false;
                enumWeather = EWeather.Sunny;
                break;
            case "weather_wind":
                BoatSpeed = 40f;
                Globals.WeatherTradeRouteModifier = 0.85f;
                ClickRum.doubleSpeed = false;
                ClickRum.halfSpeed = true;
                enumWeather = EWeather.Windy;
                break;
        }

        isAnimating = true;
    }

    void PlaySoundEffect()
    {
        switch (enumWeather)
        {
            case EWeather.Rainy:
                GetComponent<AudioSource>().clip = SoundEffects[0];
                break;
            case EWeather.Sunny:
                GetComponent<AudioSource>().clip = SoundEffects[1];
                break;
            case EWeather.Windy:
                GetComponent<AudioSource>().clip = SoundEffects[2];
                break;
            default:
                throw new Exception("The weather hasn't been set!");
        }

        GetComponent<AudioSource>().Play();
    }

    /// <summary>
    /// Weather: Windy
    ///Trade speed: Fast
    ///Clicking speed: Slow
    /// </summary>
    string GetToolTipText()
    {
        string clickSpeed;
        string tradeSpeed;

        switch (enumWeather)
        {
            case EWeather.Rainy:
                clickSpeed = "Fast";
                tradeSpeed = "Slow";
                break;
            case EWeather.Sunny:
                clickSpeed = "Average";
                tradeSpeed = "Average";
                break;
            case EWeather.Windy:
                clickSpeed = "Slow";
                tradeSpeed = "Fast";
                break;
            default:
                throw new Exception("A weather pattern has not been accounted for or weather is null");
        }

        return string.Format("Weather: {0} \n Trade Speed: {1} \n Click Speed: {2}", enumWeather.ToString(), tradeSpeed, clickSpeed);
    }

    void OnMouseEnter()
    {
        if (!Globals.TutorialDisabled || Globals.TutorialDisable == ETutorialStage.ChangeWeather)
        {
            transform.Find("ToolTip").gameObject.SetActive(true);
        }
    }

    void OnMouseExit()
    {
        if (!Globals.TutorialDisabled || Globals.TutorialDisable == ETutorialStage.ChangeWeather)
        {
            transform.Find("ToolTip").gameObject.SetActive(false);
        }

        // HIDE TUTORIAL - CHANGE WEATHER
        if (Globals.TutorialDisable == ETutorialStage.ChangeWeather)
        {
            GameObject.Find("TutorialUI").GetComponent<TutorialController>().HideTutorial(ETutorialStage.ChangeWeather);
        }
    }
}
