﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpdateScore : MonoBehaviour {

    const string lightRum = "Light";
    const string darkRum = "Dark";
    public string RumType;

	// Update is called once per frame
	void Update () {
	    if (RumType == lightRum)
        {
            GetComponent<Text>().text = Globals.FormatLargeNumber(Globals.LightRumQuantity);
        }
        else if (RumType == darkRum)
        {
            GetComponent<Text>().text = Globals.FormatLargeNumber(Globals.DarkRumQuantity);
        }
        else
        {
            throw new System.Exception("You've got a RumType that doesn't exist: " + RumType);
        }
	}
}
