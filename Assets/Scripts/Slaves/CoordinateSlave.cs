﻿using UnityEngine;

/// <summary>
/// Holds the coordinates for the route the boats take
/// </summary>
public class CoordinateSlave
{
    /// <summary>
    /// Get the coordinates for this boat to follow
    /// </summary>The trade route to map coordinates to</param>
    public static void GetCoordinates(FollowRoute boatRoute)
    {
        switch (boatRoute.routeName)
        {
            case ERouteName.JamVen:
                boatRoute.WayPointList = JamVen;
                break;
            case ERouteName.AntUK:
                boatRoute.WayPointList = AntUK;
                break;
            case ERouteName.AntTri:
                boatRoute.WayPointList = AntTri;
                break;
            case ERouteName.AntPue:
                boatRoute.WayPointList = AntPue;
                break;
            case ERouteName.PueTri:
                boatRoute.WayPointList = PueTri;
                break;
            case ERouteName.TriVen:
                boatRoute.WayPointList = TriVen;
                break;
            case ERouteName.HisVen:
                boatRoute.WayPointList = HisVen;
                break;
            case ERouteName.HisPue:
                boatRoute.WayPointList = HisPue;
                break;
            case ERouteName.NicVen:
                boatRoute.WayPointList = NicVen;
                break;
            case ERouteName.HisJam:
                boatRoute.WayPointList = HisJam;
                break;
            case ERouteName.BahPue:
                boatRoute.WayPointList = BahPue;
                break;
            case ERouteName.JamNic:
                boatRoute.WayPointList = JamNic;
                break;
            case ERouteName.BahUSA:
                boatRoute.WayPointList = BahUSA;
                break;
            case ERouteName.BahCub:
                boatRoute.WayPointList = BahCub;
                break;
            case ERouteName.CubHis:
                boatRoute.WayPointList = CubHis;
                break;
            case ERouteName.MexNic:
                boatRoute.WayPointList = MexNic;
                break;
            case ERouteName.JamMex:
                boatRoute.WayPointList = JamMex;
                break;
            case ERouteName.CubMex:
                boatRoute.WayPointList = CubMex;
                break;
            case ERouteName.CubUSA:
                boatRoute.WayPointList = CubUSA;
                break;
            case ERouteName.MexUSA:
                boatRoute.WayPointList = MexUSA;
                break;
        }
    }

    private static Vector3[] JamVen = new Vector3[6] { new Vector3(104, -268, 0), new Vector3(-120, -180, 0), new Vector3(-120, -33, 0), new Vector3(-95, -33, 0), new Vector3(-95, -180, 0), new Vector3(136, -263, 0) } ;
    private static Vector3[] AntUK = new Vector3[6] { new Vector3(559, -11, 0), new Vector3(556, 207, 0), new Vector3(650, 293, 0), new Vector3(650, 267, 0), new Vector3(589, 207, 0), new Vector3(589, -40, 0) };
    private static Vector3[] AntTri = new Vector3[6] { new Vector3(583, -74, 0), new Vector3(617, -120, 0), new Vector3(609, -346, 0), new Vector3(582, -342, 0), new Vector3(546, -291, 0), new Vector3(546, -96, 0) };
    private static Vector3[] AntPue = new Vector3[4] { new Vector3(537, -49, 0), new Vector3(407, -16, 0), new Vector3(408, 17, 0), new Vector3(537, -26, 0) };
    private static Vector3[] PueTri = new Vector3[6] { new Vector3(559, -355, 0), new Vector3(534, -348, 0), new Vector3(328, -171, 0), new Vector3(346, -28, 0), new Vector3(364, -28, 0), new Vector3(388, -171, 0) };
    private static Vector3[] TriVen = new Vector3[6] { new Vector3(547, -383, 0), new Vector3(386, -363, 0), new Vector3(214, -290, 0), new Vector3(237, -271, 0), new Vector3(393, -315, 0), new Vector3(549, -370, 0) };
    private static Vector3[] HisVen = new Vector3[5] { new Vector3(158, -324, 0), new Vector3(158, -125, 0), new Vector3(158, -14, 0), new Vector3(178, -14, 0), new Vector3(178, -324, 0) };
    private static Vector3[] HisPue = new Vector3[4] { new Vector3(311, 6, 0), new Vector3(222, 63, 0), new Vector3(243, 83, 0), new Vector3(329, 21, 0) };
    private static Vector3[] NicVen = new Vector3[4] { new Vector3(69, -300, 0), new Vector3(-208, -300, 0), new Vector3(-375, -180, 0), new Vector3(-208, -274, 0) };
    private static Vector3[] HisJam = new Vector3[4] { new Vector3(56, -18, 0), new Vector3(56, -38, 0), new Vector3(-64, -38, 0), new Vector3(-64, -18, 0) };
    private static Vector3[] BahPue = new Vector3[6] { new Vector3(356, 20, 0), new Vector3(300, 197, 0), new Vector3(-1, 215, 0), new Vector3(-1, 235, 0), new Vector3(315, 212, 0), new Vector3(376, 20, 0) };
    private static Vector3[] JamNic = new Vector3[4] { new Vector3(-180, -15, 0), new Vector3(-165, -30, 0), new Vector3(-380, -143, 0), new Vector3(-400, -130, 0)};
    private static Vector3[] BahUSA = new Vector3[4] { new Vector3(-154, 382, 0), new Vector3(-232, 336, 0), new Vector3(-247, 351, 0), new Vector3(-169, 397, 0) };
    private static Vector3[] BahCub = new Vector3[4] { new Vector3(-142, 244, 0), new Vector3(-112, 244, 0), new Vector3(-47, 152, 0), new Vector3(-77, 152, 0) };
    private static Vector3[] CubHis = new Vector3[4] { new Vector3(43, 45, 0), new Vector3(-114, 45, 0), new Vector3(-114, 65, 0), new Vector3(43, 65, 0) };
    private static Vector3[] MexNic = new Vector3[4] { new Vector3(-480, -102, 0), new Vector3(-548, 83, 0), new Vector3(-522, 96, 0), new Vector3(-461, -102, 0) };
    private static Vector3[] JamMex = new Vector3[4] { new Vector3(-193, -8, 0), new Vector3(-530, 134, 0), new Vector3(-470, 134, 0), new Vector3(-193, 12, 0) };
    private static Vector3[] CubMex = new Vector3[4] { new Vector3(-542, 149, 0), new Vector3(-542, 169, 0), new Vector3(-447, 241, 0), new Vector3(-447, 221, 0) };
    private static Vector3[] CubUSA = new Vector3[4] { new Vector3(-315, 309, 0), new Vector3(-282, 315, 0), new Vector3(-266, 247, 0), new Vector3(-301, 247, 0) };
    private static Vector3[] MexUSA = new Vector3[4] { new Vector3(-598, 176, 0), new Vector3(-373, 438, 0), new Vector3(-359, 411, 0), new Vector3(-569, 176, 0) };
}
