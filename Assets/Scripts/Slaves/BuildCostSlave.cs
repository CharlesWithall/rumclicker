﻿/// <summary>
/// Holds the prices for all the upgrades
/// </summary>
public class BuildCostSlave {

    private const double million = 1000000;
    private const double billion = 1000000000;
    private const double trillion = 1000000000000;
    private const double quadrillion = 1000000000000000;
    private const double quintillion = 1000000000000000000;

    /// <summary>
    /// Gets the price from the lists
    /// </summary>
    /// <param name="country">The country one is building in</param>
    /// <param name="index">The stage through the upgrade flow the player is at</param>
    /// <returns>The cost in rum of the upgrade</returns>
    public static double GetCost(ECountry country, uint index)
    {
        switch (country)
        {
            case ECountry.Antigua:
                return antigua[index];
            case ECountry.Bahamas:
                return bahamas[index];
            case ECountry.Cuba:
                return cuba[index];
            case ECountry.Hispaniola:
                return hispaniola[index];
            case ECountry.Jamaica:
                return jamaica[index];
            case ECountry.Mexico:
                return mexico[index];
            case ECountry.Nicaragua:
                return nicaragua[index];
            case ECountry.Puerto_Rico:
                return puertorico[index];
            case ECountry.Trinidad:
                return trinidad[index];
            case ECountry.United_States:
                return unitedstates[index];
            case ECountry.Venezuela:
                return venezuela[index];
            default:
                throw new System.Exception("An illegal country has been selected. Country: " + country.ToString());
        }
    }

    private static double[] antigua = new double[6] { 1000, 5000, 50000, 5 * million, 500 * million, 50 * billion };
    private static double[] bahamas = new double[6] { 200 * million, billion, 10 * billion, 1 * trillion, 100 * trillion, 10 * quadrillion };
    private static double[] cuba = new double[6] { 3300 * million, 16.5 * billion, 165 * billion, 16.5 * trillion, 1.65 * quadrillion, 165 * quadrillion };
    private static double[] hispaniola = new double[6] { 1.3 * million, 6.5 * million, 65 * million, 6.5 * billion, 650 * billion, 65 * trillion };
    private static double[] jamaica = new double[6] { 51 * billion, 255 * billion, 2.55 * trillion, 255 * trillion, 25.5 * quadrillion, 2.55 * quintillion };
    private static double[] mexico = new double[6] { 140 * trillion, 700 * trillion, 7 * quadrillion, 700 * quadrillion, 70 * quintillion, 7000 * quintillion };
    private static double[] nicaragua = new double[6] { 10 * trillion, 50 * trillion, 500 * trillion, 50 * quadrillion, 5 * quintillion, 500 * quintillion };
    private static double[] puertorico = new double[6] { 120000, 600000, 6 * million, 600 * million, 60 * billion, 6 * trillion };
    private static double[] trinidad = new double[6] { 11000, 55000, 550000, 55 * million, 55 * billion, 550 * billion };
    private static double[] unitedstates = new double[6] { 750 * billion, 3.75 * trillion, 37.5 * trillion, 3.75 * quadrillion, 375 * quadrillion, 37.5 * quintillion };
    private static double[] venezuela = new double[6] { 14 * million, 70 * million, 700 * million, 70 * billion, 7 * trillion, 700 * trillion };
}
