﻿using UnityEngine;

/// <summary>
/// Holds all the things that the tutorial script needs to know about
/// </summary>
public static class TutorialSlave {

    /// <summary>
    /// Gets the dialogue to display in the middle of the screen
    /// </summary>
    /// <param name="tutorialStage">The tutorial stage to display</param>
    /// <returns>A string representing the display text</returns>
    public static string GetTutorialText(ETutorialStage tutorialStage)
    {
        if (tutorialStage == ETutorialStage.ChangeWeather)
        {
            switch (GameObject.Find("WeatherIcon").GetComponent<WeatherSwitcher>().enumWeather)
            {
                case EWeather.Rainy:
                    return tutorialText[23];
                case EWeather.Sunny:
                    return tutorialText[24];
                case EWeather.Windy:
                    return tutorialText[25];
                default:
                    throw new System.Exception("An illegal weather type has been selected");
            }
        }
        else
        {
            return tutorialText[(int)tutorialStage];
        }
    }

    /// <summary>
    /// Gets the position of the tutorial 'highlight' to display on the screen e.g. the highlight might be higlighting the weather icon so will be in the bottom right
    /// </summary>
    /// <param name="tutorialStage">The tutorial stage to display</param>
    /// <returns>A transform.position to position the highlight</returns>
    public static Vector3 GetTutorialPosition(ETutorialStage tutorialStage)
    {
        return tutorialHighlightsPositions[(int)tutorialStage];
    }

    /// <summary>
    /// Gets a list of all gameobjects that need to be enabled during this tutorial stage
    /// </summary>
    /// <param name="tutorialStage">The tutorial stage to display</param>
    /// <returns>A string array representing the gameobjects to enable</returns>
    public static string[] GetTutorialGameObjects(ETutorialStage tutorialStage)
    {
        string[] sArray = new string[2] { tutorialGameObjects[(int)tutorialStage, 0], tutorialGameObjects[(int)tutorialStage, 1] };
        return sArray;
    }

    /// <summary>
    /// Gets the size of the tutorial 'highlight' to display on the screen e.g. the highlight might be bigger for the weather icon than the upgrades menu
    /// </summary>
    /// <param name="tutorialStage">The tutorial stage to display</param>
    /// <returns>A transform.scale to scale the highlight</returns>
    public static Vector3 GetTutorialSize(ETutorialStage tutorialStage)
    {
        switch(tutorialStage)
        {
            case ETutorialStage.BuyTradeRoute:
                return new Vector3(169.4f, 38.16f, 1);
            case ETutorialStage.MoveHome:
                return new Vector3(50, 50, 1);
            case ETutorialStage.BuyUpgrade:
                return new Vector3(178.7f, 51.21f, 1);
            default:
                return new Vector3(143.663f, 143.663f, 1);
        }
    }

    /// <summary>
    /// Gets if this tutorial stage has been shown or not
    /// </summary>
    /// <param name="tutorialStage">The tutorial stage to display</param>
    /// <returns>true if this tutorial stage has been shown, false otherwise</returns>
    public static bool GetTutorialStageShown(ETutorialStage tutorialStage)
    {
        return tutorialStageShown[(int)tutorialStage];
    }

    /// <summary>
    /// Sets the tutorial stage shown to true - this tutorial has been shown
    /// </summary>
    /// <param name="tutorialStage">The tutorial stage to display</param>
    public static void SetTutorialStageShown(ETutorialStage tutorialStage)
    {
        tutorialStageShown[(int)tutorialStage] = !tutorialStageShown[(int)tutorialStage];
    }

    private static string[] tutorialText;
    private static Vector3[] tutorialHighlightsPositions;
    private static string[,] tutorialGameObjects;
    private static bool[] tutorialStageShown;

    /// <summary>
    /// Constructor - builds the lists
    /// </summary>
    static TutorialSlave()
    {
        tutorialText = new string[32];
        tutorialText[(int)ETutorialStage.BuyLightRum] = "You tried distilling some rum in your garage.\nIt's terrible...\nClick the rum bottle to start selling!";
        tutorialText[(int)ETutorialStage.BuyTradeRoute] = "Your friends tell you that your rum is fantastic!\nThey recommend you try selling it in the caribbean.\nYou get the impression they just want to get rid of you...\nClick the trade route to start trading anyway!";
        tutorialText[(int)ETutorialStage.MoveHome] = "No-one in the caribbean likes your rum.\nThe weather is much nicer over here though...\nClick the islands to move to Antigua!";
        tutorialText[23] = "Rats! It's raining... It's like you're back in Britain\nYour trade routes have slowed down...\nYour clicking yields more rum though!";
        tutorialText[24] = "What lovely weather!\nThis is why you moved to the caribbean.\nCareful if it changes\nIt may affect your trade routes or your clicking";
        tutorialText[25] = "Whoosh! Look how fast your boats are going!\nYour trade routes have sped up...\nYour clicking yields less rum though!";
        tutorialText[(int)ETutorialStage.BuyDarkRum] = "Your rum is still vile...\nYou decide to diversify your portfolio.\nYou can now manufacture dark rum!\nYou can click on a boat to change the rum it produces.";
        tutorialText[(int)ETutorialStage.BuyUpgrade] = "Your boats are leaking...\nFortunately someone is willing to improve them in exchange for your rum!\nYou can only assume they've never tried it...";

        tutorialHighlightsPositions = new Vector3[10];
        tutorialHighlightsPositions[(int)ETutorialStage.BuyLightRum] = new Vector3(-789, 364, 1);
        tutorialHighlightsPositions[(int)ETutorialStage.BuyTradeRoute] = new Vector3(808, 496, 1); // change size to 169.4, 38.16, 1
        tutorialHighlightsPositions[(int)ETutorialStage.MoveHome] = new Vector3(566, -57, 1); // change size to 50, 50, 1
        tutorialHighlightsPositions[(int)ETutorialStage.ChangeWeather] = new Vector3(807, -379, 1);
        tutorialHighlightsPositions[(int)ETutorialStage.BuyDarkRum] = new Vector3(-784, -376, 1);
        tutorialHighlightsPositions[(int)ETutorialStage.BuyUpgrade] = new Vector3(808, 84, 0);

        tutorialGameObjects = new string[10, 2];
        tutorialGameObjects[(int)ETutorialStage.BuyLightRum, 0] = "Rum";
        tutorialGameObjects[(int)ETutorialStage.BuyLightRum, 1] = "RumContainer";
        tutorialGameObjects[(int)ETutorialStage.BuyTradeRoute, 0] = "TradeRoutePanel";
        tutorialGameObjects[(int)ETutorialStage.BuyTradeRoute, 1] = null;
        tutorialGameObjects[(int)ETutorialStage.MoveHome, 0] = "Antigua";
        tutorialGameObjects[(int)ETutorialStage.MoveHome, 1] = null;
        tutorialGameObjects[(int)ETutorialStage.ChangeWeather, 0] = "WeatherContainer";
        tutorialGameObjects[(int)ETutorialStage.ChangeWeather, 1] = "WeatherIcon";
        tutorialGameObjects[(int)ETutorialStage.BuyDarkRum, 0] = "DarkRum";
        tutorialGameObjects[(int)ETutorialStage.BuyDarkRum, 1] = "DarkRumContainer";
        tutorialGameObjects[(int)ETutorialStage.BuyUpgrade, 0] = "UpgradesPanel";
        tutorialGameObjects[(int)ETutorialStage.BuyUpgrade, 1] = null;

        tutorialStageShown = new bool[10];
    }
}
