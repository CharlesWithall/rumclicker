﻿using UnityEngine;

/// <summary>
/// Handles the boats' movement across the map
/// </summary>
public class FollowRoute : MonoBehaviour {

    public Vector3[] WayPointList;
    public int currentWayPoint;
    public float speed = 25f;
    public ERouteName routeName;
    public int zeroIndexedBoatNumber;
    public GameObject Weather;
    private Vector3 targetWayPoint;
    public bool isDarkRum = false;

    /// <summary>
    /// Executes on startup of application
    /// </summary>
    void Start()
    {
        Weather = GameObject.Find("WeatherIcon");
        currentWayPoint = zeroIndexedBoatNumber;
        CoordinateSlave.GetCoordinates(this);

        transform.localPosition = WayPointList[zeroIndexedBoatNumber];
        targetWayPoint = transform.localPosition;
    }

    /// <summary>
    /// Executes once per frame
    /// </summary>
    void Update()
    {
        speed = Weather.GetComponent<WeatherSwitcher>().BoatSpeed;
        GetComponent<Animator>().speed = speed / 30;

        if (targetWayPoint == null)
        {
            targetWayPoint = WayPointList[currentWayPoint];
        }

        SailToPoint();
    }

    /// <summary>
    /// Move the boat towards the next target co-ordinate
    /// </summary>
    void SailToPoint()
    {
        if (transform.localPosition == targetWayPoint)
        {
            if (currentWayPoint == WayPointList.Length - 1)
            { 
                currentWayPoint = 0;
            }
            else
            {
                currentWayPoint++;
            }

            targetWayPoint = WayPointList[currentWayPoint];

            UpdateDirectionOfTravel();
        }

        transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetWayPoint, speed * Time.deltaTime);
    }

    /// <summary>
    /// 0 = North
    /// 1 = North-East
    /// 2 = East
    /// 3 = South-East
    /// 4 = South
    /// Changes the boat animation to point the boat in the correct direction
    /// </summary>
    /// <param name="direction">The direction of travel for the boat</param>
    void SetDirectionOfTravel(EDirection direction)
    {
        UpdateSortingOrder(direction);

        AnimationSlave animationSlave = GameObject.Find("AnimationSlave").GetComponent<AnimationSlave>();

        int darkModifer = isDarkRum ? 5 : 0;

        switch (direction)
        {
            case EDirection.North:
                GetComponent<Animator>().runtimeAnimatorController = animationSlave.AnimationPool[0 + darkModifer];
                break;
            case EDirection.NorthEast:
                GetComponent<Animator>().runtimeAnimatorController = animationSlave.AnimationPool[1 + darkModifer];
                break;
            case EDirection.East:
                GetComponent<Animator>().runtimeAnimatorController = animationSlave.AnimationPool[2 + darkModifer];
                break;
            case EDirection.SouthEast:
                GetComponent<Animator>().runtimeAnimatorController = animationSlave.AnimationPool[3 + darkModifer];
                break;
            case EDirection.South:
                GetComponent<Animator>().runtimeAnimatorController = animationSlave.AnimationPool[4 + darkModifer];
                break;
            case EDirection.SouthWest:
                GetComponent<Animator>().runtimeAnimatorController = animationSlave.AnimationPool[3 + darkModifer];
                break;
            case EDirection.West:
                GetComponent<Animator>().runtimeAnimatorController = animationSlave.AnimationPool[2 + darkModifer];
                break;
            case EDirection.NorthWest:
                GetComponent<Animator>().runtimeAnimatorController = animationSlave.AnimationPool[1 + darkModifer];
                break;
        }

        if (direction == EDirection.NorthWest || direction == EDirection.West || direction == EDirection.SouthWest)
        {
            transform.localRotation = new Quaternion(transform.localRotation.x, 180, transform.localRotation.z, transform.localRotation.w);
        }
        else
        {
            transform.localRotation = new Quaternion(transform.localRotation.x, 0, transform.localRotation.z, transform.localRotation.w);
        }
    }

    /// <summary>
    /// Calculates what the direction of the boat facing should be
    /// </summary>
    void UpdateDirectionOfTravel()
    {
        int previousWayPoint;

        if (currentWayPoint == 0)
        {
            previousWayPoint = WayPointList.Length - 1;
        }
        else
        {
            previousWayPoint = currentWayPoint - 1;
        }

        float xDiff = WayPointList[currentWayPoint].x - WayPointList[previousWayPoint].x;
        float yDiff = WayPointList[currentWayPoint].y - WayPointList[previousWayPoint].y;

        if (xDiff == 0)
        {
            if ( yDiff > 0 )
            {
                SetDirectionOfTravel(EDirection.North);
            }
            else if (yDiff < 0)
            {
                SetDirectionOfTravel(EDirection.South);
            }
        }
        else
        {
            float ratioDifference = Mathf.Abs(yDiff) / Mathf.Abs(xDiff);

            if (ratioDifference > 2.4 && yDiff > 0)
            {
                SetDirectionOfTravel(EDirection.North);
            }
            else if (ratioDifference < 0.4125 && xDiff > 0)
            {
                SetDirectionOfTravel(EDirection.East);
            }
            else if (ratioDifference > 2.4 && yDiff < 0)
            {
                SetDirectionOfTravel(EDirection.South);
            }
            else if (ratioDifference < 0.4125 && xDiff < 0)
            {
                SetDirectionOfTravel(EDirection.West);
            }
            else if (xDiff > 0 && yDiff > 0)
            {
                SetDirectionOfTravel(EDirection.NorthEast);
            }
            else if (xDiff > 0 && yDiff < 0)
            {
                SetDirectionOfTravel(EDirection.SouthEast);
            }
            else if (xDiff < 0 && yDiff < 0)
            {
                SetDirectionOfTravel(EDirection.SouthWest);
            }
            else if (xDiff < 0 && yDiff > 0)
            {
                SetDirectionOfTravel(EDirection.NorthWest);
            }
        }
    }

    /// <summary>
    /// Changes which boat should be in front of which
    /// </summary>
    /// <param name="direction">The direction the boat is pointing</param>
    void UpdateSortingOrder(EDirection direction)
    {
        if (direction == EDirection.NorthWest || direction == EDirection.SouthWest || direction == EDirection.West || direction == EDirection.North)
        {
            GetComponent<SpriteRenderer>().sortingOrder = 2;
        }
        else
        {
            GetComponent<SpriteRenderer>().sortingOrder = 1;
        }        
    }
}
