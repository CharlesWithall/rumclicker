﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handles the highlight of the countries on the world map
/// </summary>
public class HighlightCountry : MonoBehaviour {

    private bool isFadingIn = false;
    public float FadeSpeed = 3;
    public ECountry country;
    
    public bool isUnlocked { get { return Globals.UnlockedCountries.Contains(country); } }

    void OnMouseUp()
    {
        if (isUnlocked && (!Globals.TutorialDisabled || Globals.TutorialDisable == ETutorialStage.MoveHome))
        {
            GameObject.Find("Flag").GetComponent<FlagScript>().currentHome = country;

            // HIDE TUTORIAL - MOVE HOME ROUTE
            if (Globals.TutorialDisable == ETutorialStage.MoveHome)
            {
                GameObject.Find("TutorialUI").GetComponent<TutorialController>().HideTutorial(ETutorialStage.MoveHome);
            }

            // SHOW TUTORIAL - Buy Dark Rum
            if (!TutorialSlave.GetTutorialStageShown(ETutorialStage.BuyDarkRum) && country == ECountry.Puerto_Rico)
            {
                GameObject.Find("TutorialUI").GetComponent<TutorialController>().ShowTutorial(ETutorialStage.BuyDarkRum);
            }
        }
    }

    void OnMouseOver()
    {
        if (isUnlocked && (!Globals.TutorialDisabled || Globals.TutorialDisable == ETutorialStage.MoveHome))
        {
            isFadingIn = true;
            SpriteRenderer sprite = GetComponent<SpriteRenderer>();

            if (sprite.color.a <= 255)
            {
                sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, Mathf.Lerp(sprite.color.a, 1, Time.deltaTime * FadeSpeed));
            }
        }
    }

    void OnMouseExit()
    {
        if (!Globals.TutorialDisabled || Globals.TutorialDisable == ETutorialStage.MoveHome)
        {
            isFadingIn = false;
        }
    }

    // Update is called once per frame
    void Update ()
    {
	    if (!isFadingIn)
        {
            SpriteRenderer sprite = GetComponent<SpriteRenderer>();
            if (sprite.color.a >= 0)
            {
                sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, Mathf.Lerp(sprite.color.a, 0, Time.deltaTime * FadeSpeed));
            }
        }
	}
}
