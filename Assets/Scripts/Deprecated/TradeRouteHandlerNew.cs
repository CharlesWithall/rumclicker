﻿using UnityEngine;
using System.Collections;

public class TradeRouteHandlerNew : MonoBehaviour {

    float timeValue = 1; // How often in seconds rum gets traded
    public long rumPerSecond = 0;
    bool boat0IsDark = false;
    bool boat1IsDark = false;
    float ticker = 0;
	
	void Update ()
    {
        if (ticker >= timeValue)
        {
            EstablishTradeRoute();
            ticker = 0;
        }
        else
        {
            ticker += Time.deltaTime;
        }
	}

    void EstablishTradeRoute()
    {
        if (boat0IsDark && boat1IsDark)
        {
            Globals.DarkRumQuantity += rumPerSecond;
        }
        else if (!boat0IsDark && !boat1IsDark)
        {
            Globals.LightRumQuantity += rumPerSecond;
        }
        else
        {
            Globals.LightRumQuantity += rumPerSecond / 2;
            Globals.DarkRumQuantity += rumPerSecond / 2;
        }
    }
}
