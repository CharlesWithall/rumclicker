﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class SailUpgrade : MonoBehaviour
{
    int currentIndex = 0;
    long[] lightPrices;
    long[] darkPrices;
    string[] sailNames;

    private bool isPurchaseble { get { return ( hasLastUpgradeBeenPurchased ? false : Globals.LightRumQuantity >= lightPrices[currentIndex] && Globals.DarkRumQuantity >= darkPrices[currentIndex]); } }
    private bool hasLastUpgradeBeenPurchased = false;

    public GameObject thisButton;
    public GameObject SailsOutline;

    void Start()
    {
        lightPrices = new long[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        darkPrices = new long[10] { 1000000, 5000000, 10000000, 50000000, 100000000, 500000000, 1000000000, 5000000000, 10000000000, 5000000000 };
        sailNames = new string[11] { "Paper Sails", "Skin Sails", "Flax Sails", "Cotton Sails", "Hemp Sails", "Nylon Sails", "Polyester Sails", "Laminate Sails", "Kevlar Sails", "Carbon Fibre Sails", "Graphene Sails" };
        ChangeColour();
    }

    public void UpgradeSail()
    {
        if (isPurchaseble && !hasLastUpgradeBeenPurchased)
        {
            Globals.TradeUpgradePercentage++;
            if (currentIndex >= 5)
            {
                Globals.TradeUpgradePercentage++;
            }
            Globals.DarkRumQuantity -= darkPrices[currentIndex];
            Globals.LightRumQuantity -= lightPrices[currentIndex];
            if (currentIndex == lightPrices.Length - 1)
            {
                hasLastUpgradeBeenPurchased = true;
            }

            currentIndex++;
            ChangeColour();
            GetComponent<AudioSource>().Play();
        }
    }

    void ChangeColour()
    {
        switch (currentIndex)
        {
            case 0:
                Color32 LightGreen = new Color32(190, 210, 170, 255);
                SailsOutline.GetComponent<SpriteRenderer>().color = LightGreen;
                break;
            case 1:
                Color32 LightBrown = new Color32(210, 168, 94, 255);
                SailsOutline.GetComponent<SpriteRenderer>().color = LightBrown;
                break;
            case 2:
                Color32 Black = new Color32(64, 58, 47, 255);
                SailsOutline.GetComponent<SpriteRenderer>().color = Black;
                break;
            case 3:
                Color32 MediumGrey = new Color32(122, 122, 122, 255);
                SailsOutline.GetComponent<SpriteRenderer>().color = MediumGrey;
                break;
            case 4:
                Color32 Orange = new Color32(236, 144, 45, 255);
                SailsOutline.GetComponent<SpriteRenderer>().color = Orange;
                break;
            case 5:
                Color32 DarkBrown = new Color32(107, 64, 17, 255);
                SailsOutline.GetComponent<SpriteRenderer>().color = DarkBrown;
                break;
            case 6:
                Color32 Yellow = new Color32(128, 115, 17, 255);
                SailsOutline.GetComponent<SpriteRenderer>().color = Yellow;
                break;
            case 7:
                Color32 DarkBlue = new Color32(74, 74, 88, 255);
                SailsOutline.GetComponent<SpriteRenderer>().color = DarkBlue;
                break;
            case 8:
                Color32 LightGrey = new Color32(193, 193, 193, 255);
                SailsOutline.GetComponent<SpriteRenderer>().color = LightGrey;
                break;
            case 9:
                Color32 DarkGrey = new Color32(83, 83, 83, 255);
                SailsOutline.GetComponent<SpriteRenderer>().color = DarkGrey;
                break;
            case 10:
                Color32 LightBlue = new Color32(170, 208, 210, 255);
                SailsOutline.GetComponent<SpriteRenderer>().color = LightBlue;
                break;
            default:
                throw new System.Exception(string.Format("The index given ({0}) has no colour associated with it.", currentIndex));
        }
    }

    void Update()
    {
        transform.parent.Find("Text").gameObject.GetComponent<Text>().text = sailNames[currentIndex];

        if (hasLastUpgradeBeenPurchased)
        {
            thisButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            if (isPurchaseble)
            {
                thisButton.GetComponent<Button>().interactable = true;
            }
            else
            {
                thisButton.GetComponent<Button>().interactable = false;
            }
        }
    }
}
