﻿using UnityEngine;
using System.Collections;

/// <summary>
/// DEPRECATED
/// </summary>
public class TradeRouteHandler : MonoBehaviour {

    bool? m_changeToLight = null;
    bool? m_changeToDark = null;
    bool? m_changeToHalf = null;    
    uint? m_rumPerSecond = null;
    public float UnitOfWaitTime = 1;
    ERouteName? m_routeName;
    public AudioClip[] SoundEffects;
    

    public void ChangeCoroutineVariables(ERouteName routeName, uint? rumPerSecond = null, bool? changeToLight = null, bool? changeToDark = null, bool? changeToHalf = null)
    {
        RevertMessageVariablesToDefault();
        m_routeName = routeName;
        m_changeToLight = changeToLight;
        m_changeToDark = changeToDark;
        m_changeToHalf = changeToHalf;
        m_rumPerSecond = rumPerSecond;
    }

    void RevertMessageVariablesToDefault()
    {
        m_changeToLight = null;
        m_changeToDark = null;
        m_changeToHalf = null;
        m_routeName = null;
        m_rumPerSecond = null;
    }

    public void StartTradeRouteCoroutine(ERouteName routeName, uint rumPerSecond, bool becomesDarkRum)
    {
        PlaySoundEffect();
        StartCoroutine(EstablishTradeRoute(routeName, rumPerSecond, becomesDarkRum));
    }

    private void PlaySoundEffect()
    {
        System.Random rand = new System.Random();
        GetComponent<AudioSource>().clip = SoundEffects[rand.Next(0, SoundEffects.Length)];
        GetComponent<AudioSource>().Play();
    }

    IEnumerator EstablishTradeRoute(ERouteName routeName, uint rumPerSecond, bool becomesDarkRum)
    {
        ERouteName co_routeName = routeName;
        uint co_rumPerSecond = rumPerSecond;
        bool co_becomesDarkRum = becomesDarkRum;
        bool co_produceDarkRum = false;

        while (true)
        {
            if (routeName == m_routeName)
            {
                // Do stuff
                if (m_changeToDark != null)
                {
                    co_produceDarkRum = true;
                }
                if (m_changeToLight != null)
                {
                    co_produceDarkRum = false;
                }
                if (m_changeToHalf != null)
                {
                    if (co_becomesDarkRum)
                    {
                        co_produceDarkRum = true;
                    }
                    else
                    {
                        co_produceDarkRum = false;
                    }
                }
                if (m_rumPerSecond != null)
                {
                    co_rumPerSecond = rumPerSecond;
                } 
            }

            if (co_produceDarkRum)
            {
                Globals.DarkRumQuantity += co_rumPerSecond;
            }
            else
            {
                Globals.LightRumQuantity += co_rumPerSecond;
            }
           
            yield return new WaitForSeconds(UnitOfWaitTime);
        }       
    }
}
