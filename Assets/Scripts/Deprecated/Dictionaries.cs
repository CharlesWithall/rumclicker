﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Dictionaries {

	public static Dictionary<ERouteName, double> LightPrices;
    public static Dictionary<ERouteName, double> DarkPrices;
    public static Dictionary<ERouteName, uint> RumPerSecond;

    static Dictionaries()
    {
        InitLightPrices();
        InitDarkPrices();
        InitRumPerSecond();
    }

    private static void InitLightPrices()
    {
        LightPrices.Add(ERouteName.AntPue, 1100);
        LightPrices.Add(ERouteName.AntTri, 100);
        LightPrices.Add(ERouteName.AntUK, 15);
    }

    private static void InitDarkPrices()
    {
        DarkPrices.Add(ERouteName.AntPue, 3);
        DarkPrices.Add(ERouteName.AntTri, 2);
        DarkPrices.Add(ERouteName.AntUK, 1);
    }

    private static void InitRumPerSecond()
    {
        RumPerSecond.Add(ERouteName.AntPue, 47);
        RumPerSecond.Add(ERouteName.AntTri, 8);
        RumPerSecond.Add(ERouteName.AntUK, 1);
    }
}
