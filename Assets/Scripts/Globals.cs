﻿using System;
using System.Collections.Generic;

/// <summary>
/// All globals that need to be accessed by everything
/// </summary>
public static class Globals {

    public static double LightRumQuantity;
    public static double DarkRumQuantity;

    public static double WeatherTradeRouteModifier = 1;

    public static double HullUpgradeMultiplier = 1;
    public static double TradeUpgradePercentage = 0;

    public static Dictionary<ECountry, uint> CountryDevelopment;

    public static List<ECountry> UnlockedCountries;

    public static ETutorialStage TutorialDisable = ETutorialStage.NONE;
    public static bool TutorialDisabled { get { return TutorialDisable != ETutorialStage.NONE; } }

    /// <summary>
    /// Instantiates lists/dictionaries
    /// </summary>
    static Globals()
    {
        UnlockedCountries = new List<ECountry>();
        UnlockedCountries.Add(ECountry.Great_Britain);

        CountryDevelopment = new Dictionary<ECountry, uint>();
        foreach (ECountry country in Enum.GetValues(typeof(ECountry)))
        {
            CountryDevelopment.Add(country, 0);
        }
    }

    /// <summary>
    /// Changes number to a more readable string
    /// </summary>
    /// <param name="rumQuantity">The number to change</param>
    /// <returns>A readable string interpretation of the number</returns>
    public static string FormatLargeNumber(double rumQuantity)
    {
        const long million = 1000000;
        const long billion = 1000000000;
        const long trillion = 1000000000000;
        const long quadrillion = 1000000000000000;

        // e.g 999,999,999,999
        // e.g 123,456,789
        double numToFormat = rumQuantity;

        // numToFormat = numToFormat - (numToFormat % 0.001M); - use this somehow?

        if (rumQuantity >= quadrillion)
        {
            numToFormat = Math.Round((numToFormat / quadrillion) - 0.00049, 3);
            return string.Format("{0} quadrillion", numToFormat.ToString());
        }
        else if (rumQuantity >= trillion)
        {
            numToFormat = Math.Round((numToFormat / trillion) - 0.00049, 3);
            return string.Format("{0} trillion", numToFormat.ToString());
        }
        else if (rumQuantity >= billion)
        {
            numToFormat = Math.Round((numToFormat / billion) - 0.00049, 3);
            return string.Format("{0} billion", numToFormat.ToString());
        }
        else if (rumQuantity >= million)
        {
            numToFormat = Math.Round((numToFormat / million) - 0.00049, 3);
            return string.Format("{0} million", numToFormat.ToString());
        }        
        else
        {
            return string.Format("{0:#,###0}", numToFormat);
        }
    }
}
