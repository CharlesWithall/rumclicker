﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A Damped Harmonic Oscillation script
/// </summary>
public class DampedHarmonicOscillation : MonoBehaviour {

    public ETutorialStage tutorialStage;
    public bool isGettingBigger = false;
    private bool directionChanged = false;
    private bool mouseStuffHappening = false;
    private float colliderRadius;

    Vector3 baseSize;
    public float sizeIncrease = 15;
    float amplitude = 20;
    float time = 0;
    public float lengthOfProcess = 3;

    float currentSize;

    /// <summary>
    /// The action to perform when the mouse is over the item
    /// </summary>
    void OnMouseOver()
    {
        if (!mouseStuffHappening && (!Globals.TutorialDisabled || Globals.TutorialDisable == tutorialStage))
        {
            isGettingBigger = true;
        }
    }

    /// <summary>
    /// The action to perform when the mouse leaves the space over the selected item
    /// </summary>
    void OnMouseExit()
    {
        if (!Globals.TutorialDisabled || Globals.TutorialDisable == tutorialStage)
        {
            mouseStuffHappening = false;
            isGettingBigger = false;
        }
    }

    /// <summary>
    /// The action to perform when the mouse button is depressed
    /// </summary>
    void OnMouseDown()
    {
        if (!Globals.TutorialDisabled || Globals.TutorialDisable == tutorialStage)
        {
            mouseStuffHappening = true;
            isGettingBigger = false;
        }
    }

    /// <summary>
    /// The action to perform when the mouse button is released
    /// </summary>
    void OnMouseUp()
    {
        if (!Globals.TutorialDisabled || Globals.TutorialDisable == tutorialStage)
        {
            mouseStuffHappening = true;
            isGettingBigger = true;
        }
    }

    /// <summary>
    /// Executes on start of application
    /// </summary>
    void Start()
    {
        baseSize = transform.localScale;
        time = lengthOfProcess;
    }

    /// <summary>
    /// Executes once a frame
    /// </summary>
	void Update ()
    {
        if (directionChanged != isGettingBigger)
        {
            time = 0;
            directionChanged = isGettingBigger;
        }

        if (isGettingBigger)
        {
            GetBigger();
        }
        else
        {
            GetSmaller();
        }
	}

    /// <summary>
    /// Makes the objects get larger using damped harmonic oscillation
    /// </summary>
    void GetBigger()
    {
        if (time <= lengthOfProcess)
        {
            currentSize = amplitude * Mathf.Exp((-time * (amplitude / lengthOfProcess)) / 3) * Mathf.Sin(time * (amplitude / lengthOfProcess) * Mathf.PI);
            time += Time.deltaTime;
            transform.localScale = new Vector3(baseSize.x + sizeIncrease + currentSize, baseSize.y + sizeIncrease + currentSize, 1);
        }
    }

    /// <summary>
    /// Makes the objects get smaller using damped harmonic oscillation
    /// </summary>
    void GetSmaller()
    {
        if (time <= lengthOfProcess)
        {
            currentSize = amplitude * Mathf.Exp((-time * (amplitude / lengthOfProcess)) / 3) * Mathf.Sin(time * (amplitude / lengthOfProcess) * Mathf.PI);
            time += Time.deltaTime;
            transform.localScale = new Vector3(baseSize.x + currentSize, baseSize.y + currentSize, 1);
        }
    }
}
